const express = require('express');
const bodyParser = require("body-parser");

const { Post } = require('./database/sequelize');

const app = express();

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS");
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.post("/api/posts", (req, res, next) => {
    Post.create({title: req.body.title, content: req.body.content})
    .then(createdPost => {
        res.status(201).json({
            message: "Post added successfully",
            postId: createdPost._id
          });
    });
  });

app.get('/api/posts', (req, res, next) => {
    Post.findAll({ raw: true }).then(posts => {
        console.log(posts);
        res.status(200).json({
            message: 'Post sent with success!',
            posts: posts
        });
    });
});

app.delete("/api/posts/:id", (req, res, next) => {
    Post.destroy({ where: {id: req.params.id} }).then(result => {
      res.status(200).json({ message: "Post deleted!" });
    });
  });

  app.get("/:id", (req, res, next) => {
    Post.findOne({ where: { id: req.params.id } }).then(post => {
      if (post) {
        res.status(200).json(post);
      } else {
        res.status(404).json({ message: "Post not found!" });
      }
    });
  });

  app.put("/:id", (req, res, next) => {
    Post.update(
      { title: req.params.title, content: req.params.content},
      { where: {
          id: req.params.id
        } 
      }
      ).then(post => {
      if (post) {
        res.status(200).json({ message: "Update successful!" });
      } else {
        res.status(404).json({ message: "Post not found!" });
      }
    });
  });

module.exports = app;
