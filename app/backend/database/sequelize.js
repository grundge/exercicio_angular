const Sequelize = require('sequelize');
const PostModel = require('../models/post');

/*const sequelize = new Sequelize({
  // The `host` parameter is required for other databases
  // host: 'localhost'
  dialect: 'sqlite',
  storage: './postsDatabase'
});*/

const sequelize = new Sequelize('postsDatabase', 'postsUser', '1234', {
  host: 'localhost',
  port: 1433,
  dialect: 'mssql',
  dialectOptions: {
      options: {
          useUTC: false,
          dateFirst: 1,
          trustServerCertificate: true,
          encrypt: true
      },
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
  }
});

/*const sequelize = new Sequelize('postsDatabase', 'postsUser', '1234', {
  host: 'localhost',
  port: 5432,
  dialect: 'postgres',
  dialectOptions: {
    // Your pg options here
  }
});*/

const Post = PostModel(sequelize, Sequelize)

sequelize.sync({ force: true })
  .then(() => {
    console.log(`Database & tables created!`)
  });

module.exports = {
  Post
}