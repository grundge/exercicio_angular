import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCreateBootstrapComponent } from './post-create-bootstrap.component';

describe('PostCreateBootstrapComponent', () => {
  let component: PostCreateBootstrapComponent;
  let fixture: ComponentFixture<PostCreateBootstrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCreateBootstrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCreateBootstrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
