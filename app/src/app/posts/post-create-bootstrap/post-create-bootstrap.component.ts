import { Component } from '@angular/core';

@Component({
  selector: 'app-post-create-bootstrap',
  templateUrl: './post-create-bootstrap.component.html',
  styleUrls: ['./post-create-bootstrap.component.css']
})
export class PostCreateBootstrapComponent {

  newEnteredValue = '';
  newPost = '';  


  onAddedPost() {
    this.newPost = this.newEnteredValue;
  }

}
